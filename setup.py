from setuptools import setup

setup(
    name='scut',
    version='0.0.1',
    packages=['scut', 'scut.databases', 'scut.websites'],
    install_requires=[
        'bunt>=0.0.1',
        'docker==4.1.0',
        'pymongo==3.9.0',
        'requests==2.21.0',
        'redis==3.3.11'
    ]
)
