# scut

Scut is a library to be used in conjunction with `bunt`. It is mainly used
as a place to dump configurations for services and abstractions for working
with those services.

# Supported Servcies 

1. MongoDB: `scut/databases/mongodb.py`
2. Redis: `scut/databases/redis.py`


# Usage

```
import unittest
from bunt.wrapper import Bunt
from scut.databases import MongoContainer
from pymongo import MongoClient


bunt = Bunt()


class MyTestCase(unittest.TestCase):

    @bunt.test_case([MongoContainer])
    def test_mongo_container(self, mongo_client: MongoClient):
        # ... you can now use `mongo_client` to talk to the mongo db instance.
        pass

```
